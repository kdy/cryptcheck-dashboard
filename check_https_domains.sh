#!/bin/bash
P=$(pwd)
cd /cryptcheck
echo -n "{ " >> $P/public/result.json
for i in $DOMAINS
do
    printf "Analyzing %-32s " $i
    GRADE=$(bin/check_https.rb $i | grep Grade | head -n 1 | sed -e "s/^.*49m\(.*\).\[0m/\1/")
    echo "$GRADE"
    echo "$i: $GRADE" >> $P/public/result.yml
    echo -n "  \"$i\": \"$GRADE\", " >> $P/public/result.json
done
echo "}" >> $P/public/result.json
sed -i -e "s@, }@ }@" $P/public/result.json
