export interface Entry {
    website: string;
    note: string;
}
