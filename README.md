# Automated cryptcheck dashboard

This is a tool to create a dashboard containing [Cryptcheck](http://cryptcheck.fr/) scores for your websites.
It uses the Docker image of [Cryptcheck](https://git.imirhil.fr/aeris/cryptcheck), adapted to add the tools to build the VueJS frontend.

This project aims to be run in [GitlabCI](https://docs.gitlab.com/ee/ci/README.html) (but you can run it manually, have a look at the [.gitlab-ci.yml](https://framagit.org/framasoft/cryptcheck/blob/master/.gitlab-ci.yml) file).

## How to use

Fork this project on a Gitlab instance and create a `DOMAINS` CI environment variable filled with a list of domains to check then launch a pipeline and the result will be published on [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/).

### DOMAINS variable

Here's a valid content for the `DOMAINS` environment variable:
```
example.org
example.net
```

## VueJS frontend

The frontend will be built automatically in GitlabCI, but here's some instructions if you want to hack it.

### Setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
